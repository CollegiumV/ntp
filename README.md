ntp
===
Configures ntp for both clients and servers.

Supported Operating Systems
---------------------------
* CentOS 7
* Void Linux

Requirements
------------
No Requirements

Role Variables
--------------
```
---
firewall_ntp_services:
  - name: ntp
    description: 'Network Time Protocol daemon'
    priority: 1
    zones: '{{ ntp_firewall_zones }}'
    rules:
      - protocol: udp
        port: 123
```

Role Defaults
-------------
```
---
ntp_driftfile: /var/lib/ntp/ntp.drift
ntp_servers:
  - 192.168.42.3
ntp_restrict:
  - 'default ignore'
ntp_statistics:
  - loopstats
  - peerstats
  - clockstats
ntp_firewall_zones:
  - '{{ firewall_default_zone }}'
```

Dependencies
------------
* site-base

Testing
-------
Tests can be found in the `tests` directory. Current tests validate the following:

* yamllint
* Syntax check
* Provisioning
* ntp access after provisioning

### Local Testing
Local testing can be done by running the `docker-compose.yml` file found in the `tests` directory

### CI Testing
CI testing configuration can be found in `.gitlab-ci.yml` in the root directory

License
-------
ISC

Author Information
------------------
CV Admins <cvadmins@utdallas.edu>
